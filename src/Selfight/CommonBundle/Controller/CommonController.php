<?php

namespace Selfight\CommonBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class CommonController extends Controller
{

    /**
     * Throws a NotFoundException, which is used to create a 404 type error page
     * @param   string  $msg    Message to show
     * @return   NotFoundException  
     */
    public function create404E($msg = NULL)
    {
        
        $msg = ($msg) ? $msg : '404';
        return $this->createNotFoundException($msg);
        
    }
    
    /**
     * Get a parameter from parameters list.
     * @param   string  $parameter  Name of the parameter.
     * @return  mixed   Value for selected parameter.
     */
    public function getParameter($parameter)
    {
        
        return $this->container->getParameter($parameter);
        
    }
    
    /**
     * Returns value for current execution environment.
     * @return  string  Name of environment.
     */
    public function getEnv()
    {
        
        return $this->getParameter('kernel.environment');
        
    }
    
    /**
     * Creates a response object tor redirect request to $route.
     * @param   string  $route  Name of the route.
     * @param   array   $params List of extra parameters for generate path for route.
     * @return  object  Response with the redirection headers.
     */
    public function SRedirect($route, $params = array())
    {
        
        return $this->redirect($this->generateUrl($route, $params));
        
    }
    
    
    public function getSession($key)
    {
        
        return $this->get('session')->get($key);
    
        
    }
    
    public function setSession($key, $value)
    {
        
        return $this->get('session')->set($key, $value);
        
    }
    
    public function toArray($obj)
    {
        
        return json_decode(json_encode($obj), true);
        
    }
    
    public function translate($msg, $msgParams)
    {
        
        $t = $this->get('translator');
        
        return $t->trans($msg, $msgParams);
        
    }
    
    public function setMessage($type, $msg, $msgParams = array(), $extras = array())
    {
        
        $tMsg = $this->translate($msg, $msgParams);
        
        $this->get('session')->getFlashBag()->add(
            'notice',
            array(
                'status' => $type,
                'msg' => $tMsg,
                'extras' => $extras,
            )
        );
        
    }
    
    

}
