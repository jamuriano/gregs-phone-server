<?php

    namespace Selfight\CommonBundle\Services;
    
    /**
     * 
     */
    class CRUDService
    {
        
        private $common = NULL;
        private $doctrine = NULL;
        private $em = NULL;
        
        /**
         * 
         */
        public function __construct($common)
        {
            
            $this->common = $common;
            $this->doctrine = $common->getService('doctrine');
            
        }
        
        public function setEntityManager($connection = 'default')
        {
            
            $this->em = $this->doctrine->getEntityManager($connection);
            return $this;
            
        }
        
        public function getEntityManager()
        {
            
            if($this->em == NULL){
                
                $this->setEntityManager();    
                
            }
            
            return $this->em;
            
        }
        
        public function create($entity) 
        {
            
            if($this->em == NULL){
                $this->setEntityManager();
            }
            
            if(is_array($entity)){
                
                foreach($entity as $thisEntity){
                    
                    // todo Validation
                    $this->em->persist($thisEntity);
                    
                }
                
            }else{
                
                // todo Validation
                $this->em->persist($entity);
                
            }
            
            $this->em->flush();
            
        }
        
        public function update()
        {
            if($this->em == NULL){
                $this->setEntityManager();
            }
            
            // todo Validation
            
            
            $this->em->flush();
            
        }
        
        public function delete($entity) 
        {
            
            if($this->em == NULL){
                $this->setEntityManager();
            }
            
            if(is_array($entity)){
                
                foreach($entity as $thisEntity){
                    
                    $this->em->remove($thisEntity);
                    
                }
                
            }else{
                
                $this->em->remove($entity);
                
            }
            
            $this->em->flush();
            
        }
        
        
    }