<?php

    namespace Selfight\CommonBundle\Services;

    /**
     * 
     */
    class CommonService
    {
        
        private $container = NULL;
        
        /**
         * 
         */
        public function __construct($container)
        {
            $this->container = $container;
        }
        
        public function getService($serviceName)
        {
            return $this->container->get($serviceName);
        }
        
        public function getContainer()
        {
            return $this->container;
        }
        
    }