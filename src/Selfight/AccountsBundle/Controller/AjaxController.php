<?php

namespace Selfight\AccountsBundle\Controller;

use Selfight\CommonBundle\Controller\CommonController as Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse as Response;

class AjaxController extends Controller
{
    public function loginRequestAction(Request $request)
    {
        
        $login = $request->request->get('login');
        $paswd = $request->request->get('paswd');
        
        $service = $this->get('accountsService');
        
        $user = $service->getCredentials($login, $paswd);
        
        if(!$user){
            $data = [
                'success' => FALSE,
                'user' => $login,
                'pasw' => $paswd,
            ];
        } else {
            $data = [
                'success' => TRUE,
                'userToken' => $user->getCreationToken(),
            ];
        }
        
        $response = new Response();
        $response->setData($data);
        $response->setCallback('f');
        
        return $response;
        
    }
}
