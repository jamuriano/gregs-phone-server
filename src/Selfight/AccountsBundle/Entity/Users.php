<?php

namespace Selfight\AccountsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Users
 */
class Users
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $creationToken;

    /**
     * @var \DateTime
     */
    private $creationDate;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $pswd;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set creationToken
     *
     * @param string $creationToken
     * @return Users
     */
    public function setCreationToken($creationToken)
    {
        $this->creationToken = $creationToken;

        return $this;
    }

    /**
     * Get creationToken
     *
     * @return string 
     */
    public function getCreationToken()
    {
        return $this->creationToken;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     * @return Users
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime 
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Users
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set pswd
     *
     * @param string $pswd
     * @return Users
     */
    public function setPswd($pswd)
    {
        $this->pswd = $pswd;

        return $this;
    }

    /**
     * Get pswd
     *
     * @return string 
     */
    public function getPswd()
    {
        return $this->pswd;
    }
}
