<?php

namespace Selfight\AccountsBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * 
 */
class UsersRepository extends EntityRepository
{
   
    public function getCredentialsForUser($login, $password)
    {
        
        $qb = $this->_em->createQueryBuilder();
        
        $qb->select('u')
            ->from('SelfightAccountsBundle:Users', 'u')
            ->andWhere('UPPER(u.email) = :email')
            ->andWhere('u.pswd = :pswd')
            ->setMaxResults(1);
            
        $qb->setParameter('email', strtoupper($login));
        $qb->setParameter('pswd', $password);
            
        try{
            return $qb->getQuery()->getSingleResult();
        } catch(\Doctrine\ORM\NoResultException $e) {
            return FALSE;
        }
        
    }
   
}