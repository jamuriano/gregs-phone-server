<?php

namespace Selfight\AccountsBundle\Services;

/**
 * 
 */
class AccountsService
{
    
    private $common = NULL;
    
    /**
     * 
     */
    public function __construct($common)
    {
        $this->common = $common;
    }
    
    public function getCredentials($login, $password)
    {
        
        $em = $this->common->getService('crudService')->getEntityManager();
        $repository = $em->getRepository('SelfightAccountsBundle:Users');
        
        return $repository->getCredentialsForUser($login, $password);
        
    }
    
}